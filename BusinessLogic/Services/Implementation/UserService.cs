﻿using System;
using System.Linq;
using BusinessLogic.UnitOfWork;

namespace BusinessLogic.Services.Implementation
{
    public class UserService: BaseService<User.User, long>, IUserService
    {
        public UserService(IUnitOfWorkFactory unitOfWorkFactory)
            : base(unitOfWorkFactory)
        {
        }
        public User.User CheckAndGetUser(string userName, string password)
        {
            if (GetAll().Any(u => u.UserName == userName && u.Password == password))
            {
                return GetByQuery(u => u.UserName == userName).FirstOrDefault();
            }
            return null;
        }
    }
}
