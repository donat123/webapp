﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic.UnitOfWork;

namespace BusinessLogic.Services.Implementation
{
    public class BaseService<TEntity, TKey>: IBaseService<TEntity, TKey>
        where TEntity: BaseEntity<TKey>
        where TKey: struct
    {
        IGenericRepository<TEntity, TKey> _repository;
        IUnitOfWork _uow;

        public BaseService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _uow = unitOfWorkFactory.Create();
            _repository = _uow.Repository<TEntity, TKey>();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _repository.GetAll();
        }

        public void Add(TEntity entity)
        {
            _repository.Add(entity);
            _uow.SaveChanges();
        }

        public void Add(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                _repository.Add(entity);
            }
            _uow.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            entity = _repository.GetById(entity.Id);
            _repository.Delete(entity);
            _uow.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            entity = _repository.GetById(entity.Id);
            _repository.Update(entity);
            _uow.SaveChanges();
        }
        
        public IEnumerable<TEntity> GetByQuery(Func<TEntity, bool> query)
        {
            return _repository.GetByQuery(query);
        }
    }
}
