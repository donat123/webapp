﻿
namespace BusinessLogic.Services
{
    public interface IUserService: IBaseService<User.User, long>
    {
        User.User CheckAndGetUser(string userName, string password);


    }
}
