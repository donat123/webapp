﻿using System;
using System.Collections.Generic;

namespace BusinessLogic.Services
{
    public interface IBaseService<TEntity, TKey>
        where TEntity : BaseEntity<TKey>
        where TKey : struct
    {
        IEnumerable<TEntity> GetAll();

        IEnumerable<TEntity> GetByQuery(Func<TEntity, bool> query);

        void Add(TEntity entity);

        void Add(IEnumerable<TEntity> entity);

        void Delete(TEntity entity);

        void Update(TEntity entity);
    }
}
