﻿
namespace BusinessLogic.User
{
    public class User : BaseEntity<long>
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public UserRole Role { get; set; }
    }
}
