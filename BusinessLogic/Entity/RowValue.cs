﻿using BusinessLogic.Product;

namespace BusinessLogic.Entity
{
    public class RowValue: BaseEntity<long>
    {
        public long Key { get; set; }

        public string Value { get; set; }
    }
}
