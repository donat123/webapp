﻿namespace BusinessLogic.Product
{
    public class CustomColumn : BaseEntity<long>
    {
        public long TypeId { get; set; }

        public string Name { get; set; }

        public string DataType { get; set; }
    }
}
