﻿using System.Collections.Generic;
using BusinessLogic.Entity;

namespace BusinessLogic.Product
{
    public class Entity : BaseEntity<long>
    {
        public string Name { get; set; }

        public long TypeId { get; set; }

        public ICollection<RowValue> RowValues { get; set; }
    }
}
