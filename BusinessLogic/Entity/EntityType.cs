﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Product
{
    public class EntityType : BaseEntity<long>
    {
        public string TypeName { get; set; }
    }
}
