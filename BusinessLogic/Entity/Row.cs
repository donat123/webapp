﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Entity;

namespace BusinessLogic.Product
{
    public class Row : BaseEntity<long>
    {
        public ICollection<RowValue> RowValues { get; set; }

    }
}
