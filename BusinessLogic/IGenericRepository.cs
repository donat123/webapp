﻿using System;
using System.Collections.Generic;

namespace BusinessLogic
{
    public interface IGenericRepository<TEntity, TKey>
        where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        IEnumerable<TEntity> GetByQuery(Func<TEntity, bool> query);

        TEntity GetById(TKey id);

        void Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);
    }
}
