﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models.User
{
    public class UserModel
    {
        public string UserName { get; set; }

        public bool IsAdmin { get; set; }
    }
}