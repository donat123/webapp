﻿
namespace MvcApplication1.Models.Entity
{
    public class ColumnModel
    {
        public long Id { get; set; }

        public long TypeId { get; set; } 

        public string Name { get; set; }

        public string DataType { get; set; }
    }
}