﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models.Entity
{
    public class EntityTypeModel
    {
        public string Name { get; set; }

        public long Id { get; set; }
    }
}