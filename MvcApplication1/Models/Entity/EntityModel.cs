﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models.Entity
{
    public class EntityModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long TypeId { get; set; }

        public IDictionary<ColumnModel, object> Row { get; set; }
    }
}