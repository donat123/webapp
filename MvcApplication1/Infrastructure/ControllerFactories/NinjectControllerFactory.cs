﻿using BusinessLogic;
using BusinessLogic.Product;
using BusinessLogic.Services;
using BusinessLogic.Services.Implementation;
using BusinessLogic.UnitOfWork;
using DataAccess;
using DataAccess.UnitOfWork;
using MvcApplication1.Infrastructure.DataAccess;
using MvcApplication1.Infrastructure.Implimentations;
using MvcApplication1.Infrastructure.Interfaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Infrastructure.Handlers;

namespace MvcApplication1.Infrastructure.ControllerFactories
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            ninjectKernel.Bind<IAuthProvider>().To<FormsAuthProvider>();
            ninjectKernel.Bind<IDbContextProvider>().To<SimpleDbContextProvider>();
            ninjectKernel.Bind(typeof(IGenericRepository<,>)).To(typeof(GenericRepository<,>));
            ninjectKernel.Bind(typeof(IBaseService<,>)).To(typeof(BaseService<,>));
            ninjectKernel.Bind<IUnitOfWorkFactory>().To<UnitOfWorkFactory>();
            ninjectKernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            ninjectKernel.Bind<ITableDataHandler>().To<TableDataHandler>();
            ninjectKernel.Bind<IUserService>().To<UserService>();
            ninjectKernel.Bind<IUserHandler>().To<UserHandler>();
            ninjectKernel.Bind<IColumnHandler>().To<ColumnHandler>();
            ninjectKernel.Bind<IEntityRepository>().To<EntityRepository>();
        }
    }
}