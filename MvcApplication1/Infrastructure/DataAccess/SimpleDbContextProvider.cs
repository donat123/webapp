﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MvcApplication1.Infrastructure.DataAccess
{
    public class SimpleDbContextProvider : IDbContextProvider
    {
        private SimpleDbContext _simpleDbContext;

        public SimpleDbContextProvider()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            _simpleDbContext = new SimpleDbContext(connectionString);
        }

        public SimpleDbContext GetSimpleDbContext()
        {
            return _simpleDbContext;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(disposing)
            {
                if(_simpleDbContext != null)
                {
                    _simpleDbContext.Dispose();
                    _simpleDbContext = null;
                }
            }
        }
    }
}