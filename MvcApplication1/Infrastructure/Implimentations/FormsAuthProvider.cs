﻿using MvcApplication1.Infrastructure.Interfaces;
using MvcApplication1.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace MvcApplication1.Infrastructure.Implimentations
{
    public class FormsAuthProvider : IAuthProvider
    {
        public UserModel Authenticate(string userName, string userPassword)
        {
            var currentUser = new UserModel();//add data logic

            if (currentUser != null)
            {
                FormsAuthentication.SetAuthCookie(userName, false);
            }

            return currentUser;
        }
    }
}