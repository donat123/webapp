﻿using BusinessLogic.Entity;
using BusinessLogic.Product;
using BusinessLogic.Services;
using MvcApplication1.Models.Entity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace MvcApplication1.Infrastructure.Handlers
{
    public class TableDataHandler : ITableDataHandler
    {
        private IBaseService<Entity, long> _entityService;
        private IBaseService<CustomColumn, long> _columnService;
        private IBaseService<EntityType, long> _entityTypeService;

        public TableDataHandler(IBaseService<Entity, long> entityService,
                                IBaseService<CustomColumn, long> columnService,
                                IBaseService<EntityType, long> entityTypeService)
        {
            _columnService = columnService;
            _entityService = entityService;
            _entityTypeService = _entityTypeService;
        }

        public void Add(EntityModel model)
        {
            var entity = new Entity();
            ConvertModelToEntity(entity, model);
            _entityService.Add(entity);
        }

        public void Add(IEnumerable<EntityModel> models)
        {
            var list = new List<Entity>();
            foreach (var model in models)
            {
                list.Add(ConvertModelToEntity(new Entity(), model));
            }

            _entityService.Add(list);
        }

        public void Update(EntityModel model)
        {
            var entity = GetEntityById(model.Id);
            ConvertModelToEntity(entity, model);
            _entityService.Update(entity);
        }

        public void Update(IEnumerable<EntityModel> models)
        {
            foreach (var model in models)
            {
                Update(model);
            }
        }

        public EntityModel GetNewModel(EntityTypeModel type)
        {
            return CreateNewModel(type);
        }

        public IEnumerable<EntityModel> GetAllByType(long typeId)
        {
            var columns = _columnService.GetByQuery(c => c.TypeId == typeId);
            var entites = _entityService.GetByQuery(e => e.TypeId == typeId);

            var models = new List<EntityModel>();

            foreach (var entity in entites)
            {
                models.Add(ConvertEntityToModel(entity, new EntityModel(), columns));
            }

            return models;
        }

        private IDictionary<ColumnModel, object> BuildModelRow(Entity entity, IEnumerable<CustomColumn> columns)
        {
            var row = new Dictionary<ColumnModel, object>();

            foreach (var column in columns)
            {
                var rowValue = entity.RowValues.Where(r => r.Key == column.Id).FirstOrDefault();
                row.Add(new ColumnModel
                {
                    Name = column.Name,
                    TypeId = column.TypeId,
                    DataType = column.DataType
                }, rowValue != null ? rowValue.Value : string.Empty);
            }

            return row;
        }

        private EntityModel ConvertEntityToModel(Entity entity, EntityModel model, IEnumerable<CustomColumn> columns)
        {
            return new EntityModel
            {
                Id = entity.Id,
                Name = entity.Name,
                TypeId = entity.TypeId,
                Row = BuildModelRow(entity, columns)
            };
        }

        private Entity ConvertModelToEntity(Entity entity, EntityModel model)
        {
            entity.TypeId = model.TypeId;
            entity.Name = model.Name;
            BuildEntityRow(entity, model);

            return entity;
        }

        private void BuildEntityRow(Entity entity, EntityModel model)
        {
            var rowValues = new List<RowValue>();
            foreach (var pair in model.Row)
            {
                rowValues.Add(new RowValue
                {
                    Key = pair.Key.Id,
                    Value = GetValueString(pair.Value)
                });
            }
            entity.RowValues = rowValues;
        }

        private string GetValueString(object value)
        {
            if (value is int || value is double || value is string)
                return value.ToString();

            if (value is bool)
                return (bool)value ? "Yes" : "No";
            if (value is Image)
            {
                var image = (Image)value;

                ImageConverter converter = new ImageConverter();
                var byteArray = converter.ConvertTo(image, typeof(byte[]));
                var base64String = Convert.ToBase64String((byte[])byteArray);

                return base64String;
            }

            throw new Exception("Unsupported format");
        }

        private EntityModel CreateNewModel(EntityTypeModel type)
        {
            return new EntityModel
            {
                Row = new Dictionary<ColumnModel, object>(),
                TypeId = type.Id
            };
        }

        private Entity GetEntityById(long id)
        {
            return _entityService.GetByQuery(e => e.Id == id).FirstOrDefault();
        }


        public EntityModel GetById(long id)
        {
            var entity = _entityService.GetByQuery(e => e.Id == id).FirstOrDefault();
            var columns = _columnService.GetByQuery(c => c.Id == entity.TypeId);
            return ConvertEntityToModel(entity, new EntityModel(), columns);
        }
    }
}