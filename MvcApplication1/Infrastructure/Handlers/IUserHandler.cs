﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcApplication1.Models.Account;
using MvcApplication1.Models.User;

namespace MvcApplication1.Infrastructure.Handlers
{
    public interface IUserHandler
    {
        UserModel LoginUser(AccountModel model);
    }
}