﻿using System.ComponentModel;
using BusinessLogic.Services;
using BusinessLogic.User;
using MvcApplication1.Infrastructure.Interfaces;
using MvcApplication1.Models.Account;
using MvcApplication1.Models.User;
using User = BusinessLogic.User.User;

namespace MvcApplication1.Infrastructure.Handlers
{
    public class UserHandler: IUserHandler
    {
        private IUserService _userService;
        private IAuthProvider _authProvider;

        public UserHandler(IUserService userService,
                           IAuthProvider authProvider)
        {
            _userService = userService;
            _authProvider = authProvider;
        }
        public UserModel LoginUser(AccountModel model)
        {
            User user = _userService.CheckAndGetUser(model.LoginName, model.Password);
            if (user != null)
            {
                _authProvider.Authenticate(user.UserName, user.Password);
                return GetUserModelFromUserEntity(user);
            }
            return null;
        }

        private UserModel GetUserModelFromUserEntity(User user)
        {
            return new UserModel
            {
                UserName = user.UserName,
                IsAdmin = user.Role == UserRole.Admin
            };
        }


    }
}