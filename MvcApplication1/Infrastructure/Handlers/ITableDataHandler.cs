﻿
using System.Collections.Generic;
using MvcApplication1.Models.Entity;

namespace MvcApplication1.Infrastructure.Handlers
{
    public interface ITableDataHandler
    {
        void Add(EntityModel model);

        void Add(IEnumerable<EntityModel> models);

        void Update(EntityModel model);

        void Update(IEnumerable<EntityModel> models);

        EntityModel GetNewModel(EntityTypeModel type);

        IEnumerable<EntityModel> GetAllByType(long typeId);

        EntityModel GetById(long id);
    }
}
