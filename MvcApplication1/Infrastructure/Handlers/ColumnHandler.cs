﻿using System.Collections.Generic;
using BusinessLogic.Product;
using BusinessLogic.Services;
using MvcApplication1.Models.Entity;
using System;
using System.Linq;

namespace MvcApplication1.Infrastructure.Handlers
{
    public class ColumnHandler : IColumnHandler
    {
        private IBaseService<CustomColumn, long> _columnService;

        public ColumnHandler(IBaseService<CustomColumn, long> columnService)
        {
            _columnService = columnService;
        }

        public void AddColumn(ColumnModel model)
        {
            var entity = GetEntityFromModel(model);
            _columnService.Add(entity);
        }

        public void UpdateColumn(ColumnModel model)
        {

        }

        public IEnumerable<ColumnModel> GetAll()
        {
            var columns = _columnService.GetAll();
            var columnsModel = new List<ColumnModel>();
            foreach (var column in columns)
            {
                columnsModel.Add(GetModelFromEntity(column));
            }

            return columnsModel;
        }


        public IEnumerable<ColumnModel> GetAllByType(long typeId)
        {
            var columns = new List<ColumnModel>();
            var entities = _columnService.GetByQuery(c => c.TypeId == typeId).ToList();
            foreach (var entity in entities)
            {
                columns.Add(GetModelFromEntity(entity));
            }

            return columns;
        }

        private ColumnModel GetModelFromEntity(CustomColumn entity)
        {
            return new ColumnModel
            {
                Id = entity.Id,
                Name = entity.Name,
                TypeId = entity.TypeId,
                DataType = entity.DataType
            };
        }

        private CustomColumn GetEntityFromModel(ColumnModel model)
        {
            return new CustomColumn
            {
                Name = model.Name,
                TypeId = model.TypeId,
                DataType = model.DataType
            };
        }
    }
}