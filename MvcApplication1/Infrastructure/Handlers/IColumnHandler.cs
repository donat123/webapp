﻿using System.Collections.Generic;
using MvcApplication1.Models.Entity;

namespace MvcApplication1.Infrastructure.Handlers
{
    public interface IColumnHandler
    {
        IEnumerable<ColumnModel> GetAll();

        IEnumerable<ColumnModel> GetAllByType(long typeId);

        void AddColumn(ColumnModel model);

        void UpdateColumn(ColumnModel model);
    }
}