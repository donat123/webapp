﻿using MvcApplication1.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcApplication1.Infrastructure.Interfaces
{
    public interface IAuthProvider
    {
        UserModel Authenticate(string userName, string userPassword);
    }
}
