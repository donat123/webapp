﻿
using MvcApplication1.Models.Account;
using System.Web.Mvc;
using MvcApplication1.Infrastructure.Handlers;

namespace MvcApplication1.Controllers
{
    public class AccountController : Controller
    {
        private IUserHandler _userHandler;

        public AccountController(IUserHandler userHandler)
        {
            _userHandler = userHandler;
        }
        //
        // GET: /Account/

        public ActionResult Login()
        {

            var model = new AccountModel();

            return View("LoginPage", model);
        }

        [HttpPost]
        public ActionResult Login(AccountModel model, string returnUrl)
        {
            var currentUser = _userHandler.LoginUser(model);
            
            if (currentUser != null)
            {
                HttpContext.Session.Add("CurrentUser", currentUser);

                return Redirect(returnUrl ?? Url.Action("List", "Report"));
            }
            
            return View();
        }
    }
}
