﻿
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MvcApplication1.Infrastructure.Handlers;
using MvcApplication1.Models.Entity;
using MvcApplication1.Models.User;

namespace MvcApplication1.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        private ITableDataHandler _tableDataHandler;
        private IColumnHandler _columnHandler;

        public ReportController(ITableDataHandler tableDataHandler,
            IColumnHandler columnHandler)
        {
            _tableDataHandler = tableDataHandler;
            _columnHandler = columnHandler;
        }
        //
        // GET: /Report/

        public ActionResult List()
        {
            List<EntityModel> model = (List<EntityModel>)_tableDataHandler.GetAllByType(1);
            return View(model);
        }

        public ActionResult ColumnManager()
        {
            if (HttpContext.Session["CurrentUser"] != null && ((UserModel) HttpContext.Session["CurrentUser"]).IsAdmin)
            {
                var model = _columnHandler.GetAll();
                return View(model);
            }

            return List();
        }

        public ActionResult UpdateEntity(long id)
        {
            var model = _tableDataHandler.GetById(id);
            return PartialView("EntityEditor", model);
        }

        public ActionResult AddEntity(long typeId)
        {
            var columns = _columnHandler.GetAllByType(typeId);
            var row = new Dictionary<ColumnModel, object>();
            foreach (var column in columns)
            {
                row.Add(column, "");   
            }
            var model = new EntityModel
            {
                Row = row,
                TypeId = typeId
            };
            return PartialView("EntityEditor", model);
        }

        [HttpPost]
        public ActionResult AddColumn(ColumnModel model)
        {
            if (((UserModel) HttpContext.Session["CurrentUser"]).IsAdmin)
            {
                _columnHandler.AddColumn(model);
            }
            return null;
        }

        [HttpPost]
        public ActionResult UpdateColumn(ColumnModel model)
        {
            if (((UserModel)HttpContext.Session["CurrentUser"]).IsAdmin)
            {
                _columnHandler.AddColumn(model);
            }
            return null;
        }

        [HttpPost]
        public ActionResult AddEntities(EntityModel model)
        {
            _tableDataHandler.Add(model);
            return null;
        }

        [HttpPost]
        public ActionResult UpdateEntities(EntityModel model)
        {
            _tableDataHandler.Update(model);
            return null;
        }

    }
}
