﻿using BusinessLogic.Entity;
using BusinessLogic.Product;
using BusinessLogic.User;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class SimpleDbContext : DbContext
    {

        public SimpleDbContext() { }

        public SimpleDbContext(string connectionString)
            :base(connectionString)
        {
        }
            
        public DbSet<User> Users { get; set; }

        public DbSet<Entity> Entites { get; set; }

        public DbSet<EntityType> EntityType { get; set; }

        public DbSet<RowValue> RowValues { get; set; }

        public DbSet<CustomColumn> CustomColumns { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
