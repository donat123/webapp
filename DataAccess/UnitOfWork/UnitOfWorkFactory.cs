﻿using BusinessLogic.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.UnitOfWork
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IDbContextProvider _dbContextProvider;

        public UnitOfWorkFactory(IDbContextProvider dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }

        public IUnitOfWork Create(bool autoDetectChanges = true)
        {
            return new UnitOfWork(_dbContextProvider, autoDetectChanges);
        }
    }
}
