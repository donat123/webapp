﻿using BusinessLogic;
using BusinessLogic.Entity;
using BusinessLogic.Product;
using BusinessLogic.UnitOfWork;
using BusinessLogic.User;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbContextProvider _dbContextProvider;
        private DbContext _dbContext;
        private IDictionary<string, object> _repositories;
            

        public UnitOfWork(IDbContextProvider dbContextProvider, bool autoDetectChanges)
        {
            _dbContextProvider = dbContextProvider;
            _dbContext = _dbContextProvider.GetSimpleDbContext();
            _dbContext.Configuration.AutoDetectChangesEnabled = autoDetectChanges;
            _repositories = new Dictionary<string, object>();
        }

        public void SaveChanges()
        {
            _dbContext.Configuration.AutoDetectChangesEnabled = true;
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Configuration.AutoDetectChangesEnabled = true;
        }

        public IGenericRepository<TEntity, TKey> Repository<TEntity, TKey>()
            where TEntity : BaseEntity<TKey>
            where TKey : struct
        {
            var type = typeof(TEntity).Name;

            if(!_repositories.ContainsKey(type))
            {
                var repository = GetRepositoryByType(type);
                _repositories.Add(type, repository);

            }

            return (IGenericRepository<TEntity, TKey>)_repositories[type];
        }

        private object GetRepositoryByType(string type)
        {
            object repository;
            if (type == typeof(Entity).Name)
            {
                repository = new EntityRepository(_dbContext);
            } else
            if (type == typeof(CustomColumn).Name)
            {
                repository = new GenericRepository<CustomColumn, long>(_dbContext);
            }else
            if (type == typeof(EntityType).Name)
            {
                repository = new GenericRepository<EntityType, long>(_dbContext);
            }else
            if (type == typeof(User).Name)
            {
                repository = new GenericRepository<User, long>(_dbContext);
            }else
            if (type == typeof(RowValue).Name)
            {
                repository = new GenericRepository<RowValue, long>(_dbContext);
            }
            else
            {
                throw new Exception("Can not create repository for that type");
            }

            return repository;
        }
    }
}
