﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;
using BusinessLogic.Product;

namespace DataAccess
{
    public class EntityRepository: GenericRepository<Entity, long>, IEntityRepository
    {
        public EntityRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public override IEnumerable<Entity> GetAll()
        {
            return _dbSet.Include(e => e.RowValues).ToList();
        }

        public override IEnumerable<Entity> GetByQuery(Func<Entity, bool> query)
        {
            return _dbSet.Include(e => e.RowValues).Where(query).ToList();
        }
    }
}
